PROGRAM solver_test


USE constants_mod
USE fp_solver_mod


IMPLICIT NONE


INTEGER, PARAMETER :: nsteps = 10000
INTEGER, PARAMETER :: tskip = 500


REAL(8), ALLOCATABLE :: pp(:), fp(:), np(:), gp(:), fp2_cc(:)
INTEGER :: i
REAL(8) :: my_dt, sim_t, time(0:nsteps), pmean_cgmv(0:nsteps), pmean_cc(0:nsteps), pmean_exact(0:nsteps) 
LOGICAL :: solve_status

ALLOCATE(fp(1:npbins), np(1:npbins), gp(1:npbins), pp(0:npbins+1), fp2_cc(1:npbins))

! initilize fp solver
CALL init_fp_solver()

! initialize distribution function and moments
CALL adiabatic_expansion_test_init(fp, np, gp, pp, fp2_cc)
!CALL diffusion_test_init(fp, np, gp, pp, fp2_cc)
CALL file_io(0)


! evolve distribution function
sim_t       = 0.d0
pmean_cgmv  = 0.D0
pmean_cc    = 0.D0
pmean_exact = 0.D0
time = 0.D0

CALL compute_mean_momentum(0)

DO i = 1, nsteps

    my_dt = 0.0001  ! uniform time step size
    solve_status = .FALSE.
    
    PRINT*,''   
    PRINT*,'Solving for time step #',i
    PRINT*,''   
    
    
    ! eveolve CR distribution function
    CALL solve(my_dt, fp, np, gp, fp2_cc, solve_status)
    
    ! file IO dump
    IF(MOD(i, tskip) .EQ. 0) CALL file_io(i)

    sim_t = sim_t + my_dt
    time(i) = sim_t
   
    CALL compute_mean_momentum(i)

   
    PRINT*,''
    PRINT*,'t, dt = ',i,sim_t, my_dt
    PRINT*,''
   
END DO


! destroy solver
CALL destroy_fp_solver()

DEALLOCATE(fp,np,gp,pp,fp2_cc)

PRINT*,''
PRINT*,'Test completed!'
PRINT*,''

CONTAINS


SUBROUTINE file_io(tstep)

    INTEGER, INTENT(IN) :: tstep
    INTEGER :: i
    CHARACTER(LEN=300) :: filename
    CHARACTER(LEN=6) :: uniti

    WRITE(uniti,FMT='(I5.5)') tstep
    filename = TRIM('Output/fp_dump=')//TRIM(uniti)//TRIM(".dat")        
    OPEN(UNIT=17,FILE=filename, FORM = 'UNFORMATTED', ACCESS = 'STREAM')

    DO i = 1, npbins 
        WRITE(17) pp(i),fp(i),np(i),gp(i),fp2_cc(i)/(pp(i)**2)
    END DO

    CLOSE(UNIT=17)

  
END SUBROUTINE file_io
    
  
SUBROUTINE compute_mean_momentum(tstep)

    INTEGER, INTENT(IN) :: tstep
    REAL(8) :: p_cc, p_cgmv, n_cgmv, n_cc, f0_cgmv, f0_cc, dp, p
    INTEGER :: i
    
    p_cc = 0.D0
    p_cgmv = 0.D0
    n_cgmv = 0.D0
    n_cc = 0.D0
    f0_cgmv = 0.D0
    f0_cc = 0.D0
    
    DO i = 1, npbins 

        dp = pp(i+1) - pp(i)
        p = 0.5D0 *(pp(i) + pp(i+1))

        f0_cgmv = 0.5D0 * (fp(i) + fp(i+1))
        f0_cc = 0.5D0 * (fp2_cc(i) + fp2_cc(i+1))

        p_cgmv = p_cgmv + (p**3) * f0_cgmv  * dp    !gp(i)              
        p_cc = p_cc +  (p**(1)) * f0_cc * dp   
        
        
        n_cgmv = n_cgmv + (p**2) * fp(i)
        n_cc = n_cc + fp2_cc(i)
        
        
    END DO
    
    !p_cgmv = p_cgmv/1.D8
    !p_cc = p_cc/1.D8
    
    p_cgmv = p_cgmv  !/n_cgmv
    p_cc = p_cc      !/n_cc
    
    
    pmean_cgmv(tstep)  = p_cgmv
    pmean_cc(tstep)    = p_cc
    pmean_exact(tstep) = pmean_cc(0) * EXP(4.D0 * sim_t)
 
     
    PRINT*,''
    PRINT*,'N_tot CGMV = ',n_cgmv
    PRINT*,'N_tot CC   = ',n_cc
    PRINT*,''
    
    
    PRINT*,''
    PRINT*,'Mean momentum CGMV = ',p_cgmv
    PRINT*,'Mean momentum CC   = ',p_cc
    PRINT*,''
 
    IF(tstep .EQ. nsteps) THEN
    OPEN(UNIT=10,FILE='Output/pmean.dat', FORM = 'UNFORMATTED', ACCESS = 'STREAM')

    DO i = 0, nsteps 
        WRITE(10) time(i), pmean_cgmv(i), pmean_cc(i), pmean_exact(i)
    END DO
    
    CLOSE(UNIT=10)        
    
    END IF
    
    
END SUBROUTINE compute_mean_momentum 



END PROGRAM solver_test

